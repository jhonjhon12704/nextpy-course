import functools


def sum_of_digits(number):
    """
    returns sum of digits in given number
    :param number: int
    :return: int
    """
    return int(functools.reduce(sumnums, str(number), 0))


def sumnums(number1, number2):
    """
    :return sum of two given numbers
    :param number1: int
    :param number2: int
    :return: int
    """
    return int(number1) + int(number2)


def main():
    print(sum_of_digits(104))
    # prints sum of digits in number


if __name__ == '__main__':
    main()
