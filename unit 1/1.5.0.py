import functools


def longest_name(file_path):
    """
    returns the longest word in given file
    :param file_path: str
    :return: str
    """
    with open(file_path, 'r') as file:
        return max((word.strip() for word in file.readlines()))


def sum_names(file_path):
    """
    returns the sum of the length f all the words in given file
    :param file_path: str
    :return: int
    """
    with open(file_path, 'r') as file:
        return sum((len(word.strip()) for word in file.readlines()))


def shortest_names(file_path):
    """
    returns the shortest words in given file in seperate lines
    :param file_path: str
    :return: str
    """
    with open(file_path, 'r') as file:
        lines = file.readlines()
        slist = [name.strip() for name in lines if name.strip() and len(name.strip()) == (min(len(line.strip()) for line in lines))]
        return "\n".join(slist)


def length_names(file_path):
    """
    creates a file that contains the words length in file, and prints it content
    :param file_path: str
    :return: str
    """
    with open(file_path, 'r') as input_file, open("C:\\Users\\jhonj\\OneDrive\\מסמכים\\name_length.txt", 'w') as output_file:
        output_file.write('\n'.join(str(len(word.strip())) for word in input_file.read().split()))
    return "".join(open("C:\\Users\\jhonj\\OneDrive\\מסמכים\\name_length.txt", 'r').readlines())


def spec_name_length(file_path):
    """
    returns the words with specific given length from file
    :param file_path: str
    :return: str
    """
    num = int(input("enter name length: "))
    with open(file_path, 'r') as file:
        slist = [name.strip() for name in file.readlines() if name.strip() and len(name.strip()) == num]
        return "\n".join(slist)


def main():
    print(longest_name("C:\\Users\\jhonj\\OneDrive\\מסמכים\\names.txt"))
    print(sum_names("C:\\Users\\jhonj\\OneDrive\\מסמכים\\names.txt"))
    print(shortest_names("C:\\Users\\jhonj\\OneDrive\\מסמכים\\names.txt"))
    print(length_names("C:\\Users\\jhonj\\OneDrive\\מסמכים\\names.txt"))
    print(spec_name_length("C:\\Users\\jhonj\\OneDrive\\מסמכים\\names.txt"))
    # prints actions on names file


if __name__ == '__main__':
    main()
