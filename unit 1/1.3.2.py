import functools


def is_prime(number):
    """
    checks if number is prime or not
    :param number: int
    :return: bool, True or False
    """
    return number > 1 and all(number % i != 0 for i in range(2, int(number**0.5) + 1))


def main():
    print(is_prime(42))
    print(is_prime(43))
    # prints if numbers are prime


if __name__ == '__main__':
    main()
