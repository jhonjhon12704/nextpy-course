def double_letter(my_str):
    """
    returns a string with double appearences for every letter
    :param my_str: string
    :return: a string with double appearences for every letter
    """
    return ''.join(map(double_app, my_str))


def double_app(letter):
    """
    returns letter doubled
    :param letter: str
    :return: str
    """
    return letter * 2


def main():
    print(double_letter("python"))
    print(double_letter("we are the champions!"))
    # print the strings given with double appearances


if __name__ == '__main__':
    main()
