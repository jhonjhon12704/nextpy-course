def four_dividers(number):
    """
    returns a filtered list of the numbers that is devided by 4 between 1 to number
    :param number: int
    :return: list
    """
    return list(filter(is_devided_by_4, range(number)))


def is_devided_by_4(number):
    """
    returns the number if its devided by 4
    :param number: int
    :return: int
    """
    if number % 4 == 0:
        return number


def main():
    print(four_dividers(9))
    print(four_dividers(3))
# prints all the numbers between 1 and 9 or 3 that is devided by 4


if __name__ == '__main__':
    main()

