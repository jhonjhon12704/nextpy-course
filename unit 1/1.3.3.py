import functools


def is_funny(string):
    """
    returns if the string only contains h and a
    :param string: str
    :return: bool, True or False
    """
    return all(char == 'h' or char == 'a' for char in string)


def main():
    print(is_funny("hahahahahaha"))
    # prints if the string contains only a and h

if __name__ == '__main__':
    main()
