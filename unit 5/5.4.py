class IDIterator:
    def __init__(self, id):
       self._id = id#id

    def __iter__(self):
        return self

    def __next__(self):
        if self._id == 999999999:#end
            raise StopIteration
        temp = self._id + 1
        while not check_id_valid(temp):#while not valid
            temp += 1
            if self._id == 999999999:#end
                raise StopIteration
        self._id = temp#update
        return temp


def check_id_valid(id_number):
    """
    checks if id is valid with various calculations
    :param id_number: int
    :return: bool
    """
    list_num = [int(digit) for digit in str(id_number)]
    counter = 1
    list_mul=[]
    for i in list_num:
        list_mul.append(i * even_return(counter))
        counter += 1
    list_new = [digitalize(i) for i in list_mul]
    sumn = sum(list_new)
    if sumn % 10 == 0:
        return True
    return False


def even_return(num):
    """
    return value according to index
    :param num: int
    :return: int
    """
    if num % 2 == 0:
        return 2
    return 1


def digitalize(num):
    """
    returning value according to number
    :param num: int
    :return: int
    """
    if num > 9:
        return (num // 10) + (num % 10)
    return num


def generate_id(id_num):
    """
    generator to generate id from specific id
    :param id_num:
    :return:
    """
    temp = id_num + 1
    while temp <= 999999999:
        if check_id_valid(temp):
            yield temp
        temp += 1


def main():
    id = int(input("enter Id: "))
    op = str(input("Generator ot Iterator? (gen/it) ? "))
    if op == "it":
        itr = IDIterator(id)
        for i in range(10):
            print(next(itr))
    elif op == "gen":
        for i in range(10):
            id = next(generate_id(id))
            print(id)
# printing next id of given id with iterator or generator


if __name__ == '__main__':
    main()
