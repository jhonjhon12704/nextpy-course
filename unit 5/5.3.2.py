class MusicNotes:
    def __init__(self):
        self._notes_table = [
            ("La", [55, 110, 220, 440, 880]),
            ("Si", [61.74, 123.48, 246.96, 493.92, 987.84]),
            ("Do", [65.41, 130.82, 261.64, 523.28, 1046.56]),
            ("Re", [73.42, 146.84, 293.68, 587.36, 1174.72]),
            ("Mi", [82.41, 164.82, 329.64, 659.28, 1318.56]),
            ("Fa", [87.31, 174.62, 349.24, 698.48, 1396.96]),
            ("Sol", [98, 196, 392, 784, 1568])
        ]
        self._col = 0
        self._row = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._col >= len(self._notes_table[0][1]):#last column
            raise StopIteration

        frequency = self._notes_table[self._row][1][self._col]# certain frequency

        self._row += 1# row +1
        if self._row >= len(self._notes_table): # bigger than rows number
            self._row = 0
            self._col += 1

        return frequency


def main():
    notes_iter = iter(MusicNotes())
    for freq in notes_iter:
        print(freq)


if __name__ == '__main__':
    main()
