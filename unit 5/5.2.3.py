import itertools


def get_combinations(total, bills):
    """
    get combinations of dollars without dups to achieve total sum
    :param total: int
    :param bills: dict
    :return: list
    """
    all_combinations = []
    for i in range(1, 15):
        combinations = set(itertools.combinations_with_replacement(bills, i))
        for combination in combinations:
            if sum(combination) == total and legal_combination(combination, bills):
                all_combinations.append(list(combination))
    return all_combinations


def legal_combination(comb, bills):
    """
    checks if the combination is legal with the amount of dollars
    :param comb: list
    :param bills: dict
    :return: bool
    """
    for item in dict(bills).keys():
        count = 0
        for i in comb:
            if i == item:
                count += 1
        if count > dict(bills).get(item):
            return False
    return True


def main():
    bills = {20: 3, 10: 5, 5: 2, 1: 5}
    target_amount = 100

    combinations = get_combinations(target_amount, bills)
    print(len(combinations))
    # prints number of combinations


if __name__ == '__main__':
    main()


