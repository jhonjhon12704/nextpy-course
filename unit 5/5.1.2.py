import winsound


def main():
    freqs = {"la": 220,
             "si": 247,
             "do": 261,
             "re": 293,
             "mi": 329,
             "fa": 349,
             "sol": 392,
             }#dict
    notes = "sol,250-mi,250-mi,500-fa,250-re,250-re,500-do,250-re,250-mi,250-fa,250-sol,250-sol,250-sol,500"
    tavs = notes.split('-')
    gen_tav = (n.split(',') for n in tavs)#generattor to extract
    print(list(gen_tav))
    for item in gen_tav:
        winsound.Beep(freqs.get(item[0]), int(item[1]))#beep


if __name__ == '__main__':
    main()
