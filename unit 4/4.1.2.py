def translate(sentence):
    """
    translate sentence using dictionary
    :param sentence: srt
    :return: str
    """
    words = {'esta': 'is', 'la': 'the', 'en': 'in', 'gato': 'cat', 'casa': 'house', 'el': 'the'}
    return ' '.join((words.get(word) for word in sentence.split()))


def main():
    print(translate("el gato esta en la casa"))
# print translated sentence


if __name__ == '__main__':
    main()


