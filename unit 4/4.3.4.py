def get_fibo():
    """
    returns generator of fibonacci series
    :return: generator
    """
    a = 0
    b = 1
    while True:
        yield a
        a, b = b, a + b


def main():
    fibo_gen = get_fibo()
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))
    print(next(fibo_gen))


# fibonacci series


if __name__ == '__main__':
    main()
