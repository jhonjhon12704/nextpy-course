from itertools import islice


def gen_secs():
    """
    generator of seconds
    :return: generator
    """
    a = 0
    while a <= 60:
        yield a
        a += 1


def gen_minutes():
    """
    generator of minutes
    :return: generator
    """
    a = 0
    while a <= 60:
        yield a
        a += 1


def gen_hours():
    """
    generator of hours
    :return:  generator
    """
    a = 0
    while a <= 24:
        yield a
        a += 1


def gen_time():
    """
    generator of time until 01:23:45
    :return: str
    """
    sec_gen = gen_secs()
    min_gen = gen_minutes()
    hour_gen = gen_hours()
    sec = 0
    hour = 0
    min = 0
    while True:
        if hour ==1:
            if min == 23:
                if sec == 46:
                    return
        yield "%02d:%02d:%02d" % (hour, min, sec)
        sec = next(sec_gen)
        if sec == 60:
            sec_gen = gen_secs()
            min = next(min_gen)
            if min == 60:
                min_gen = gen_minutes()
                hour = next(hour_gen)


def gen_years(start=2023):
    """
    generates the years from given year until today
    :param start: int
    :return: generator
    """
    a = start
    while a <= 2023:
        yield a
        a += 1


def gen_months():
    """
    generates month
    :return: generator
    """
    a = 1
    while a <= 13:
        yield a
        a += 1


def gen_days(month, leap_year=True):
    """
    generates number of days in month according to leap years
    :param month: int
    :param leap_year: bool
    :return: generator
    """
    start = 1
    end = end_month(month, leap_year)
    while start <= end:
        yield start
        start += 1


def end_month(month, leap_year=True):
    """
    checks how much days is in month
    :param month: int
    :param leap_year: bool
    :return: int
    """
    end = 30
    if month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12:
        end = 31
    if month == 2 and leap_year:
        end = 29
    if month == 2 and not leap_year:
        end = 28
    return end


def gen_date():
    """
    generates dates and time by format
    :return: str format
    """
    year_gen = gen_years(2019)
    year = next(year_gen)
    month_gen = gen_months()
    mon = next(month_gen)
    day_gen = gen_days(mon, is_leap_year(year))
    day = next(day_gen)
    sec_gen = gen_secs()
    min_gen = gen_minutes()
    hour_gen = gen_hours()
    sec = next(gen_secs())
    hour = next(gen_hours())
    min = next(gen_minutes())
    while True:
        yield "%d/%d/%d %02d:%02d:%02d" % (day, mon, year, hour, min, sec)
        sec = next(sec_gen)
        if sec == 60:#full minute
            sec_gen = gen_secs()
            min = next(min_gen)
            if min == 60:# full hour
                min_gen = gen_minutes()
                hour = next(hour_gen)
                if hour == 24:# full day
                    hour_gen = gen_hours()
                    day = next(day_gen)
                    if day == end_month(mon, is_leap_year(year)):# full month
                        mon = next(month_gen)
                        if mon == 13:# full year
                            month_gen = gen_months()
                            year = next(year_gen)
                        day_gen = gen_days(mon, is_leap_year(year))


def is_leap_year(year):
    return year % 4 == 0 and year % 100 != 0 and year % 400 == 0


def main():
    gen = gen_date()
    i = 1
    while i % 1000000 != 0:
        next(gen)
        i += 1
        if i % 1000000 == 0:
            print(next(gen))


# counts time and date by format


if __name__ == '__main__':
    main()


