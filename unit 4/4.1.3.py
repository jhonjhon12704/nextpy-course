def first_prime_over(n):
    """
    returns first prime number after given number
    :param n: int
    :return: int
    """
    return next(i for i in range(n, n*2) if is_prime(i))


def is_prime(n):
    """
    checks if number is prime
    :param n: int
    :return: bool
    """
    # Corner case
    if n <= 1:
        return False
    # Check from 2 to n-1
    for i in range(2, n):
        if n % i == 0:
            return False
    return True


def main():
    print(first_prime_over(100000))
# print first prime number after


if __name__ == '__main__':
    main()
