from itertools import takewhile, count


def parse_ranges(ranges_string):
    """
    using generators to extract and print number ranges
    :param ranges_string: str
    :return: generator
    """

    range_gen = (r for r in ranges_string.split(','))
    number_gen = (n.split('-') for n in range_gen)
    n_gen = (x for j in number_gen for x in takewhile(lambda num: num <= int(j[1]), count(int(j[0]))))
    return n_gen


def main():
    print(list(parse_ranges("1-2,4-4,8-10")))
    print(list(parse_ranges("0-0,4-8,20-21,43-45")))
# print ranges of numbers


if __name__ == '__main__':
    main()
