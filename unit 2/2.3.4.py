class Pixel:
    """
    _x - x coordinate
    _y - y coordinate
    _red - a value between 0 and 255
    _green - a value between 0 and 255
    _blue - a value between 0 and 255
    """
    def __init__(self, x=0, y=0, red=0, green=0, blue=0):
        """initialization method"""
        self._x = x
        self._y = y
        self._red = red
        self._green = green
        self._blue = blue

    def set_coords(self, x, y):
        """updating coords"""
        self._x = x
        self._y = y

    def set_grayscale(self):
        """ calculation of avg of colors and putting avg in all the colors values"""
        avg = (self._red + self._blue + self._green)/3
        self._green = avg
        self._blue = avg
        self._red = avg

    def print_pixel_info(self):
        """prints pixel info based on values"""
        res = ""
        if self._red == 0 and self._green == 0 and self._blue > 50:
            res = "Blue"
        elif self._red == 0 and self._blue == 0 and self._green > 50:
            res = "Green"
        elif self._blue == 0 and self._green == 0 and self._red > 50:
            res = "Red"
        print("X: %d, Y: %d, Color(%d, %d, %d) %s" % (self._x, self._y, self._red, self._green, self._blue, res))


def main():
    p = Pixel(5, 6, 250)
    p.print_pixel_info()
    p.set_grayscale()
    p.print_pixel_info()
    # printing and changing pixel values


if __name__ == '__main__':
    main()
