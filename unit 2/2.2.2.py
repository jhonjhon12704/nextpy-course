class Octopus:
    """
    octopus class

    attributes:
    oname = string name
    oage = int age
    """

    def __init__(self):
        """
        initialization method
        """
        self.oname = "kooli"
        self.oage = 2

    def birthday(self):
        """updating age method"""
        self.oage = self.oage + 1

    def get_age(self):
        """returning age method"""
        return self.oage


def main():
    oc = Octopus()
    oc2 = Octopus()
    oc.birthday()
    print(oc.get_age(), oc2.get_age())
# two animals iniialization and updating ones age and print their ages


if __name__ == '__main__':
    main()
