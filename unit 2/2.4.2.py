class BigThing:
    """
    name - name str
    """

    def __init__(self, name):
        """initialization method"""
        self._name = name

    def size(self):
        """returns value of size according to the type of attribute"""
        if isinstance(self._name, int):
            return self._name
        elif isinstance(self._name, str) or isinstance(self._name, list) or isinstance(self._name, tuple):
            return len(self._name)


class BigCat(BigThing):
    """
    weight - int weight
    """

    def __init__(self, name, weight):
        BigThing.__init__(self, name)
        self._weight = weight

    def size(self):
        """returns value of size according to weight"""
        if 21 > self._weight > 15:
            return "Fat"
        elif self._weight > 20:
            return "Very Fat"
        else:
            return "OK"


def main():
    my_thing = BigThing("balloon")
    print(my_thing.size())
    cutie = BigCat("mitzy", 22)
    print(cutie.size())
    # creating big thing objects and prints their sizes


if __name__ == '__main__':
    main()


