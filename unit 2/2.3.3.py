class Octopus:
    """
    octopus class

    attributes:
    oname = string name
    oage = int age
    """
    count_animals = 0

    def __init__(self, age, name="octavio"):
        """
        initialization method
        """
        self._oname = name
        self._oage = age
        Octopus.count_animals = Octopus.count_animals + 1

    def birthday(self):
        """updating age method"""
        self._oage = self._oage + 1

    def get_age(self):
        """returning age method"""
        return self._oage

    def set_name(self, new_name):
        """updating name method"""
        self._oname = new_name

    def get_name(self):
        """returning name method"""
        return self._oname


def main():
    oc = Octopus(2, "kooli")
    oc2 = Octopus(3)
    print(oc.get_name(), oc2.get_name())
    oc2.set_name("magnivi")
    print(oc2.get_name())
    print(Octopus.count_animals)
# two animals iniialization and updating ones name and print their names and number of octupy


if __name__ == '__main__':
    main()
