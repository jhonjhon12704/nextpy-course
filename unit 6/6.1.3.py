import tkinter as tk
from idlelib import window

from PIL import ImageTk, Image


def button_click():
    # image file
    image = Image.open("C:\\Users\\jhonj\\OneDrive\\מסמכים\\happy.jpg")


    photo = ImageTk.PhotoImage(image) #photo image

    image_label = tk.Label(image=photo)
    image_label.image = photo
    image_label.pack()


def main():
    window = tk.Tk()
    window.title("My Window")

    label = tk.Label(window, text="How are you?") #text
    label.pack()

    button = tk.Button(window, text="Click Me", command=button_click) #button initialization
    button.pack()

    window.mainloop()


if __name__ == '__main__':
    main()
