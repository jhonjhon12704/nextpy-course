from file1 import GreetingCard


class BirthdayCard(GreetingCard):

    def __init__(self, sender_age=0):
        GreetingCard.__init__(self)
        self._sender_age = sender_age

    def greeting_msg(self):
        print("Happy birthdy -", self._sender_age)
        GreetingCard.greeting_msg(self)
