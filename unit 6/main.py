from file1 import GreetingCard
from file2 import BirthdayCard


def main():
    bc = BirthdayCard()
    gc = GreetingCard()
    bc.greeting_msg()
    gc.greeting_msg()


if __name__ == '__main__':
    main()

