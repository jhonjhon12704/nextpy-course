from gtts import gTTS
import os


def text_to_speech(text, filename):
    """
    converting text to mp file
    :param text: str
    :param filename: str
    :return: none
    """
    tts = gTTS(text=text, lang='en')
    tts.save(filename)


def main():
    text = "First time I'm using a package in the next.py course"
    output_filename = "output.mp3"
    text_to_speech(text, output_filename)

    # play audio file
    os.system("start " + output_filename)


if __name__ == '__main__':
    main()

