class UnderAge(Exception):
    """under age exception"""
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "not 18 yet, age: %d and number of years until joinable %d" % (self._arg, 18 - self._arg)
        """error"""

    def get_arg(self):
        return self._arg


def send_invitation(name, age):
    """
    sends invites to party to people above 18
    :param name: str
    :param age: int
    :return: prints proper message
    """
    try:
        if int(age) < 18:
            raise UnderAge(age)
    except UnderAge as e:
        print(e)
    else:
        print("You should send an invite to " + name)


def main():
    send_invitation("noam", 17)
    send_invitation("yuval", 20)
    # checking age and sending invites


if __name__ == '__main__':
    main()
