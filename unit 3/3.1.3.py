def stop_iteration_error():
    empty_iterator = iter([])
    next(empty_iterator)


def zero_division_error():
    result = 1 / 0


def assertion_error():
    assert 2 + 2 == 5


def import_error():
    import non_existent_module


def key_error():
    my_dict = {"a": 1, "b": 2}
    value = my_dict["c"]


def syntax_error():
    if True
        print("syntax")


def indentation_error():
    print("Indentation")
     print("line")


def type_error():
    result = "5" + 3


def main():
    stop_iteration_error()
    type_error()
    key_error()
    assertion_error()
    zero_division_error()
    # throw exceptions


if __name__ == '__main__':
    main()

