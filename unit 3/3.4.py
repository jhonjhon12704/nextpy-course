import re


class MyException(Exception):
    pass


class UsernameContainsIllegalCharacter(MyException):
    def __init__(self, arg, index):
        self._arg = arg
        self._index = index

    def __str__(self):
        return f"The username contains an illegal character '{self._arg}' at index {self._index}"

    def get_arg(self):
        return self._arg


class UsernameTooShort(MyException):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Illegal short username"

    def get_arg(self):
        return self._arg


class UsernameTooLong(MyException):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Illegal long username"

    def get_arg(self):
        return self._arg


class PasswordMissingCharacter(MyException):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Missing character from password"

    def get_arg(self):
        return self._arg


class PasswordMissingCharacterUpper(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Missing character from password (Uppercase)"

    def get_arg(self):
        return self._arg


class PasswordMissingCharacterLower(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Missing character from password (Lowercase)"

    def get_arg(self):
        return self._arg


class PasswordMissingCharacterNumber(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Missing character from password (Digit)"

    def get_arg(self):
        return self._arg


class PasswordMissingCharacterSpecial(PasswordMissingCharacter):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Missing character from password (Special)"

    def get_arg(self):
        return self._arg


class PasswordTooShort(MyException):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Illegal short password"

    def get_arg(self):
        return self._arg


class PasswordTooLong(MyException):
    def __init__(self, arg):
        self._arg = arg

    def __str__(self):
        return "Illegal long password"

    def get_arg(self):
        return self._arg


def has_required_characters(string):
    """
    Checks if the string has at least one uppercase letter, one lowercase letter, one digit, and one special character.
    :param string: str
    :return: True or False
    """
    pattern = r'^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[^\w\s]).*$'
    return bool(re.match(pattern, string))


def contains_special_characters(string):
    """
    Checks if the string contains at least one illegal character not letter number or _
    :param string: str
    :return: True or False
    """
    pattern = r'^[a-zA-Z0-9_]+$'
    return not bool(re.match(pattern, string))


def check_input(username, password):
    """
    Checks if the username and password are valid using exceptions
    :param username: str
    :param password: str
    :return: Prints "OK" if valid data
    """
    try:
        if 16 < len(username):
            raise UsernameTooLong(username)
        if len(username) < 3:
            raise UsernameTooShort(username)

        if contains_special_characters(username):
            index = len(username)
            for i, char in enumerate(username):
                if not char.isalnum() and char != '_':
                    index = i
                    break
            raise UsernameContainsIllegalCharacter(char, index)

        if 40 < len(password):
            raise PasswordTooLong(password)
        if len(password) < 8:
            raise PasswordTooShort(password)

        if not has_required_characters(password):
            if not re.search(r'[A-Z]', password):
                raise PasswordMissingCharacterUpper(password)
            if not re.search(r'[a-z]', password):
                raise PasswordMissingCharacterLower(password)
            if not re.search(r'\d', password):
                raise PasswordMissingCharacterNumber(password)
            if not re.search(r'[^\w\s]', password):
                raise PasswordMissingCharacterSpecial(password)
            raise PasswordMissingCharacter(password)

    except MyException as e:
        print(e)
    else:
        print("OK")


def main():
    check_input("1", "2")
    check_input("0123456789ABCDEFG", "2")
    check_input("A_a1.", "12345678")
    check_input("A_1", "2")
    check_input("A_1", "ThisIsAQuiteLongPasswordAndHonestlyUnnecessary")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")
    check_input("A_1", "4BCD3F6.1jk1mn0p")
    check_input("A_1", "abcdefghijklmnop")
    check_input("A_1", "ABCDEFGHIJLKMNOP")
    check_input("A_1", "ABCDEFGhijklmnop")
    check_input("A_1", "4BCD3F6h1jk1mn0p")
    # checks inputs of username and password


if __name__ == '__main__':
    main()



