def read_file(file_name):
    """
    opens file and prints its content based on format
    :param file_name: str
    :return: string file content format
    """
    try:
        print("__CONTENT_START__")
        with open(file_name, 'r') as file:
            print("".join(file.readlines()))

    except:
        print("__NO_SUCH_FILE__")

    finally:
        print("__CONTENT_END__")


def main():
    read_file("C:\\Users\\jhonj\\OneDrive\\מסמכים\\names.txt")
    read_file("C:\\Users\\jhonj\\OneDrive\\מסמכים\\axe.txt")
    # read from real file and not real file


if __name__ == '__main__':
    main()
